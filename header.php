<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shop-wp
 */

?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>

</head>
<body <?php body_class() ?>>

<header id="header" class="header">
    <?php
    /**
     * @hooked shop_wp_nav 10
     * @hooked shop_wp_modal_login 20
     * @hooked shop_wp_modal_register 30
     */
     do_action('header_parts'); ?>
</header>
