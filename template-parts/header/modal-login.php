<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
 <section class="sign">
        <div class="container-fluid wrapper__sign">
            <div class="row justify-content-center lol">
                <div class="col-lg-5">
                    <div class="button-close">
                        <span class="close"></span>
                    </div>
                    <div class="bg-wrapper">
                        <div class="row justify-content-center">
                            <div class="col-lg-4 mt-5">
                                <div class="tittle__sign">
                                    <h4>Авторизация</h4>
                                </div>
                            </div>
                        </div>
                        <?php get_template_part('/woocommerce/includes/parts/wc-form', 'login'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>