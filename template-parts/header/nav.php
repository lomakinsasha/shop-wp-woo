<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="container-fluid container-top">
	<div class="row">
		<div class="col-lg-12">
			<nav class="navbar navbar-light navbar-expand-lg border-bottom">
				<?php //the_custom_logo( $blog_id ); ?>
				<?php
				$logo_id = carbon_get_theme_option('shop_wp_header_logo');
				$logo = $logo_id ? wp_get_attachment_image_src($logo_id, 'full') : '';
				?>
				<?php if (is_front_page() && is_home()) :
					if ($logo_id) :
						?>
						<a class="navbar-brand" href="<?php echo home_url('/'); ?>"><img src="<?php echo $logo[0];?>" alt="" width="<?php echo $logo[1];?>" height="<?php echo $logo[2];?>"></a>
					<?php else: ?>
						<a class="navbar-brand" href="<?php echo home_url('/'); ?>">
							<?php echo carbon_get_theme_option('shop_wp_header_name')?>
						</a>
					<?php endif; ?>
				<?php else: ?>
					<a class="navbar-brand" href="<?php echo home_url('/'); ?>"><img src="<?php echo $logo[0];?>" alt="" width="<?php echo $logo[1];?>" height="<?php echo $logo[2];?>"></a>
				<?php endif; ?>
				<button class="navbar-toggler  nav-tog" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse navbar-top" id="navbarSupportedContent">
					<?php shop_wp_header_menu(); ?>
				</div>
                <?php
                    if (!is_user_logged_in()) :
                ?>
                    <a href="#" id="log-in"><span class="menu-span"><i class="fa fa-user"></i></span>Войти</a>
				<?php else:
				?>
                <p><?php
	                $current_user = get_user_by( 'id', get_current_user_id() );
					/* translators: 1: user display name 2: logout url */
					printf(
						__( '<i class="fa fa-user"></i> Не <a href="my-account">%1$s?</a> <a href="%2$s" class="log-out">выйти</a>', 'woocommerce' ),
						'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
						esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
					);
					?></p>
				<?php
				endif;
				?>

                <?php
                    if (!is_cart() && !is_checkout()) :?>
                        <div class="block-cart">
					<?php shop_wp_woo_woocommerce_cart_link(); ?>
                        <div class="bot-block col-lg-4 flex-column" style="">
                            <?php the_widget('WC_Widget_Cart', 'title=');?>
                        </div>
                </div>
                <?php
                    endif;
                ?>


		        <div class="wrapper">
					<div class="container-x">
						<form  method="POST" action="<?php esc_url( home_url( '/' ) );?>">
							<input type="text" value="<?php get_search_query();?>" name="s" class="input" placeholder="Искать" autocomplete="off ">
							<i class="fa fa-search" aria-hidden="true"></i>
						</form>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>