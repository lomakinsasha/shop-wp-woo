<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="col-md-3 col-sm-12 ml-auto d-flex flex-column end-block-footer">
	<h3 class="title-footer">social</h3>
	<p><?php echo carbon_get_theme_option('shop_wp_footer_copy');?></p>
	<div class="networks d-flex">
		<a href="https://vk.com/calligraphy.class" id="vk"><i class="fa fa-vk"></i></a>
		<a href="https://www.instagram.com/calligraphy.class/"><i class="fa fa-instagram"></i></a>
		<a href="https://www.instagram.com/calligraphy.class/"><i class="fa fa-twitter"></i></a>
	</div>
</div>
