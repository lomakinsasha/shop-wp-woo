<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<!-- Вывод подписки -->
<?php
$news = carbon_get_theme_option( 'shop_wp_footer_newsletter_show' );
if ( 'on' === $news  && is_front_page()) :
	?>
	<section class="register-block" id="sign">
		<div class="container-fluid subscriber">
			<div class="row">
				<div class="col-lg-12 d-flex justify-content-center flex-column">
					<h2 class="title-register d-flex justify-content-center ">sign up to receive our updates</h2>
					<p class="main-par d-flex justify-content-center ">Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5 m-auto">
					<form class="d-flex justify-content-center">
						<input type="email" name="login" placeholder="Your e-mail" class="email-form" >
						<input type="submit" value="add" class="btn btn-sub">
					</form>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
