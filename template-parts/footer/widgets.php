<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="col-md-2 mr-a d-flex flex-column first-block-footer">
    <?php if (is_active_sidebar('footer-1')) : ?>
        <?php dynamic_sidebar('footer-1'); ?>
    <?php endif; ?>
</div>
<div class="col-md-2 d-flex flex-column second-block-footer">
	<?php if (is_active_sidebar('footer-2')) : ?>
		<?php dynamic_sidebar('footer-2'); ?>
	<?php endif; ?>
</div>
<div class="col-md-2 d-flex flex-column third-block-footer">
	<?php if (is_active_sidebar('footer-3')) : ?>
		<?php dynamic_sidebar('footer-3'); ?>
	<?php endif; ?>
</div>
