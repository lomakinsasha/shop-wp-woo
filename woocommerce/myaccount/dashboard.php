<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
<!--            <p>--><?php
//		        /* translators: 1: user display name 2: logout url */
//		        printf(
//			        __( 'Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce' ),
//			        '<strong>' . esc_html( $current_user->display_name ) . '</strong>',
//			        esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
//		        );
//		        ?>
<!--            </p>-->

            <p class="mb-5 mt-5"><?php
		        printf(
			        __( 'Из главной страницы аккаунта вы можете посмотреть ваши <a href="%1$s" class="log-out">недавние заказы</a>, настроить <a href="%2$s" class="log-out">платежный адрес и адрес доставки</a>, а также <a href="%3$s" class="log-out">изменить пароль и основную информацию</a>.', 'woocommerce' ),
			        esc_url( wc_get_endpoint_url( 'orders' ) ),
			        esc_url( wc_get_endpoint_url( 'edit-address' ) ),
			        esc_url( wc_get_endpoint_url( 'edit-account' ) )
		        );
		        ?>
            </p>
        </div>
    </div>
</div>
<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
