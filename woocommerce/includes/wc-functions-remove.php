<?php
if ( ! defined( 'ABSPATH') ) {
    exit;
}
// Disable Styles of wc
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
// Disable widget cart button
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10 );
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );
// Disable woocommerce_breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
// Disable wc sidebar
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
// Disable wc data_tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
// swap
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
// remove rating
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
// remove filter
remove_all_filters( 'woocommerce_before_shop_loop');
// remove start
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
// remove action tittle product
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
// remove description
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);
// remove coupon
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form',10 );
// remove empty-cart
remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);