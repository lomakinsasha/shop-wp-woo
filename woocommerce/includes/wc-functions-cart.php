<?php
if ( ! function_exists( 'shop_wp_woo_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	add_filter( 'woocommerce_add_to_cart_fragments', 'shop_wp_woo_woocommerce_cart_link_fragment' );
	function shop_wp_woo_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		shop_wp_woo_woocommerce_cart_link();
		$fragments['.top-block'] = ob_get_clean();
		return $fragments;
	}
}


if ( ! function_exists( 'shop_wp_woo_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function shop_wp_woo_woocommerce_cart_link() {
		?>
        <div class="top-block ml-auto d-flex justify-content-center align-items-center" style="">
            <a class="cart-contents cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'shop-wp-woo' ); ?>">
                <span class="menu-span"><i class="fa fa-shopping-basket"></i></span><span class="txt-link-menu">Корзина (<span class="count"><?php echo wp_kses_data( WC()->cart->get_cart_contents_count() ) ;?></span>)</span>
            </a>
        </div>
		<?php
	}
}

if ( ! function_exists( 'shop_wp_woo_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function shop_wp_woo_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php shop_wp_woo_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}

if ( ! function_exists( 'woocommerce_mini_cart' ) ) {

	function woocommerce_mini_cart( $args = array() ) {

		$defaults = array(
			'list_class' => 'my_class'
		);

		$args = wp_parse_args( $args, $defaults );

		wc_get_template( 'cart/mini-cart.php', $args );
	}
}

add_action( 'woocommerce_cart_is_empty', 'shop_wp_cart_empty_message', 10);
function shop_wp_cart_empty_message() {
	echo '<p class="cart-empty title-slider">' . wp_kses_post( apply_filters( 'wc_empty_cart_message', __( 'Ваша корзина пуста', 'woocommerce' ) ) ) . '</p>';
}