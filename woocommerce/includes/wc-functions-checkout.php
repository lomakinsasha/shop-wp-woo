<?php


add_filter( 'woocommerce_checkout_fields' , 'shop_wp_woocommerce_checkout_fields', 10, 1 );

function shop_wp_woocommerce_checkout_fields($fields){

	unset($fields['billing']['billing_first_name']['class'][0]);
	unset($fields['billing']['billing_first_name']['class'][1]);
	unset($fields['billing']['billing_first_name']['class'][2]);

	unset($fields['billing']['billing_last_name']['class'][0]);
	unset($fields['billing']['billing_last_name']['class'][1]);
	unset($fields['billing']['billing_last_name']['class'][2]);

	unset($fields['billing']['billing_country']['class'][0]);
	unset($fields['billing']['billing_country']['class'][1]);
	unset($fields['billing']['billing_country']['class'][2]);

	unset($fields['billing']['billing_address_1']['class'][0]);

	unset($fields['billing']['billing_state']['class'][0]);
	unset($fields['billing']['billing_state']['class'][1]);


	unset($fields['billing']['billing_company']);
	unset($fields['shipping']['shipping_company']);


	$fields['billing']['billing_first_name']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_last_name']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_country']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_state']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_postcode']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_phone']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_email']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_city']['class'][0] = "col-md-6 mb-3";
	$fields['billing']['billing_address_1']['class'][0]="mb-3 col-md-12 order__billing";

	$fields['shipping']['shipping_first_name']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_last_name']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_country']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_state']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_city']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_postcode']['class'][0] = "col-md-6 mb-3";
	$fields['shipping']['shipping_address_1']['class'][0]="mb-3 col-md-12 order__billing";

//	$fields['account']['account_username']

	$fields['billing']['billing_address_1']['type']="textarea"; //меняем тип поля с input на textarea
	$fields['shipping']['shipping_address_1']['type']="textarea"; //меняем тип поля с input на textarea

	 // переименовываем поле

	unset($fields['billing']['billing_address_2']); //удаляем Подъезд, этаж и т.п.
	unset($fields['shipping']['shipping_address_2']); //удаляем Подъезд, этаж и т.п.

//	get_pr($fields);
	return $fields;
}


