<?php
if ( ! defined( 'ABSPATH') ) {
	exit;
}

add_action( 'woocommerce_before_main_content', 'shop_wp_archive_wrapper_start', 40);
function shop_wp_archive_wrapper_start() {
?>
 <div class="container-fluid">
<!--	 <div class="row">-->
<?php
}

add_action( 'woocommerce_after_main_content', 'shop_wp_archive_wrapper_end', 40);
function shop_wp_archive_wrapper_end() {
?>
<!--	</div>-->
	</div>
<?php
}

add_action( 'woocommerce_after_shop_loop', 'shop_wp_archive_pagination_wrapper_start', 5);
function shop_wp_archive_pagination_wrapper_start() {
	?>
	<section class="pagination" id="pagination">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 d-flex justify-content-center">
<?php
}

add_action( 'woocommerce_after_main_content', 'shop_wp_archive_pagination_wrapper_end', 5);
function shop_wp_archive_pagination_wrapper_end() {
	?>
	</div>
	</div>
	</div>
	</section>
	<?php
}

add_filter( 'woocommerce_show_page_title', 'shop_wp_hide_tittle_shop' );
function shop_wp_hide_tittle_shop( $hide ) {
	if (is_shop()) {
		$hide = false;
	}
	return $hide;
}

add_action('woocommerce_shop_loop_item_title', 'shop_wp_loop_item_tittle');
function shop_wp_loop_item_tittle() {
	echo '<h2 class="woocommerce-loop-product__title name-product">' . get_the_title() . '</h2>';
}

// Wrapper price
add_action( 'woocommerce_after_shop_loop_item_title', 'shop_wp_start_wrapper_price', 5);
add_action( 'woocommerce_after_shop_loop_item_title', 'shop_wp_end_wrapper_price', 15);

function shop_wp_start_wrapper_price() {
    ?>
    <div class="price">
<?php
}

function shop_wp_end_wrapper_price() {
    ?>
    </div>
<?php
}

/**
 * Exclude products from a particular category on the shop page
 */
function shop_wp_product_query_action( $q ) {
    if (!is_shop()) {
	    $tax_query = $q->get( 'tax_query' );
	    $tax_query[] = array(
		    'taxonomy' => 'product_cat',
		    'field' => 'slug',
		    'terms' => $q->query_vars['product_cat'],
		    'include_children' => false,
	    );
	    $q->set( 'tax_query', $tax_query );
    }
}

add_action( 'woocommerce_product_query', 'shop_wp_product_query_action' );


