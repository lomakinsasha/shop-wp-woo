<?php

add_action( 'woocommerce_widget_shopping_cart_buttons', 'my_woocommerce_widget_shopping_cart_proceed_to_checkout', 10 );
function my_woocommerce_widget_shopping_cart_proceed_to_checkout() {
	echo '<a href="' . esc_url( wc_get_checkout_url() ) . '" class="btn btn-default">' . esc_html__( 'Checkout', 'woocommerce' ) . '</a>';
}

function shop_wp_woocommerce_scripts() {
	wp_enqueue_style( 'shop-wp-woocommerce-style', get_template_directory_uri() . '/woocommerce.css', array('shop-wp-bootsrap-style') );
	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';
	wp_add_inline_style( 'shop-wp-woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'shop_wp_woocommerce_scripts' );

