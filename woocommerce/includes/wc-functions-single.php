<?php
if ( ! defined( 'ABSPATH') ) {
	exit;
}

add_action( 'woocommerce_before_main_content', 'shop_wp_add_breadcrumbs', 20 );
function shop_wp_add_breadcrumbs() {
	?>
	<section class="category" id="category">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 mr-auto">
				<span class="categories">
					<?php woocommerce_breadcrumb(); ?>
				</span>
				</div>
			</div>
		</div>
	</section>
	<?php
}

add_action( 'woocommerce_before_single_product', 'shop_wp_wrapper_product_start', 5 );
function shop_wp_wrapper_product_start() {
	?>
	<div class="single-section">
	<?php
}

add_action( 'woocommerce_after_single_product', 'shop_wp_wrapper_product_end', 5 );
function shop_wp_wrapper_product_end() {
	?>
	</div>
	<?php
}

add_action( 'woocommerce_before_single_product_summary', 'shop_wp_wrapper_images_start', 5 );
function shop_wp_wrapper_images_start() {
	?>
	<section class="galery" id="galery">
	<div class="container-fluid">
	<div class="row">
	<div class="col-lg-12">
	<div class="ppp">
	<?php
}

add_action( 'woocommerce_before_single_product_summary', 'shop_wp_wrapper_images_end', 25 );
function shop_wp_wrapper_images_end() {
	?>
	</div>
	</div>
	</div>
	</div>
	</section>
	<?php
}

add_action( 'woocommerce_before_single_product_summary', 'shop_wp_wrapper_entry_start', 30 );
function shop_wp_wrapper_entry_start() {
	?>
	<section class="description" id="description">
	<div class="container-fluid">
	<div class="row">
	<div class="col-lg-12 justify-content-center d-flex flex-column align-items-center">
	<?php
}

add_action( 'woocommerce_after_single_product_summary', 'shop_wp_wrapper_entry_end', 5 );
function shop_wp_wrapper_entry_end() {
	?>
	</div>
	</div>
	</div>
	</section>
	<?php
}

add_action('woocommerce_single_product_summary', 'shop_wp_template_single_meta', 11 );
function shop_wp_template_single_meta() {
	wc_get_template( 'single-product/meta.php' );
}

add_action('woocommerce_single_product_summary', 'shop_wp_template_single_meta_category', 40 );
function shop_wp_template_single_meta_category() {
	wc_get_template( 'single-product/meta-category.php' );
}