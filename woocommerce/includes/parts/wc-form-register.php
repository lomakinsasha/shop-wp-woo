<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
wc_print_notices();
?>
<form method="post" class="register">
	<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

	<div class="row justify-content-center">
		<div class="col-lg-8 mt-4">
			<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="text" class="form-control" name="username"
			       id="reg_username"
			       value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) :
				       ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
		</div>

	<?php endif; ?>

		<div class="col-lg-8 mt-4">
		<label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span
				class="required">*</span></label> <input type="email"
		                                                 class="form-control" name="email" id="reg_email"
		                                                 value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) :
			                                                 ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
		</div>

	<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

		<div class="col-lg-8 mt-4">
			<label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
			<input type="password" class="form-control" name="password"
			       id="reg_password"/>
		</div>

	<?php endif; ?>
		<div class="col-lg-8 mt-5 mb-5">
			<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
			<button type="submit" class="btn orange btn-lg btn-block" name="register"
			        value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
		</div>
	</div>
</form>
