<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<form class="woocommerce-form woocommerce-form-login login" method="post">

	<?php do_action( 'woocommerce_login_form_start' ); ?>

    <div class="row justify-content-center">
		<div class="col-lg-8 mt-5">
            <label for="username"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" class="input-text form-control" name="username" id="username" autocomplete="username" />
		</div>
		<div class="col-md-8 mb-3 mt-5">
			<div class="pas-for d-flex justify-content-between">
                <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot">
					<?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?>
                </a>

			</div>
            <input class="input-text form-control" type="password" name="password" id="password" autocomplete="current-password" />
        </div>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-4 mt-5 mb-2 d-flex flex-column justify-content-center">
		<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline wc-size-label justify-content-center"> <input
				class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox"
				id="rememberme" value="forever"/> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
		</label>
		<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
		<button type="submit" class="btn orange btn-lg btn-block" name="login"
			value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>

		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-lg-4 mb-5">
			<div class="new-user">
				<a href="#" id="new-user" class="forgot">У меня нет аккаунта</a>
			</div>
		</div>
	</div>

	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>

