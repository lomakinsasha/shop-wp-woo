<?php
/**
 *Template Name: Main
 */

get_header();
?>

    <section class="title-section" id="title-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center flex-column">
                        <?php if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
                            <!-- Цикл WordPress -->
                        <h1 class="main-title d-flex justify-content-center "><?php the_title(); ?></h1>
                        <p class="main-par d-flex justify-content-center "><?php the_content(); ?></p>
                        <?php } } else { ?>
                    <p>Записей нет.</p>
                    <?php } ?>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 m-auto head-bt">
                            <a href="#sign" class="btn header__btn" id="to-reg">Сообщить о новых поступлениях</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="products-grid">
        <div class="container-fluid">
            <div class="row justify-content-md-column">
                <?php
                $args = array(
	                'taxonomy' => 'product_cat',
	                'orderby'    => 'count',
	                'order'      => 'DESC',
	                'hide_empty' => false
                );

                $product_categories = get_terms( $args );

                $count = count($product_categories);

                if ( $count > 0 ){
	                foreach ( $product_categories as $product_category ) {
		                $thumbnail_id = get_post_thumbnail_id( $product_category->term_id );
		                echo '<div class="catalogue-item col-lg-3 col-md-4 col-sm-6 col-12">';
		                echo '<a href="' . get_term_link( $product_category ) . '" class="catalogue-item-link">';
		                echo '<div class="catalogue-item-img">';
		                woocommerce_subcategory_thumbnail( $product_category );
		                echo '</div>';
		                echo '<div class="text-block-grid">';
		                echo '<p class="catalog-item-title lato-bold">' . $product_category->name . '</p>';
		                echo '</div>';
		                echo '</a>';
		                echo '</div>';
	                }
                }
                ?>
            </div>
        </div>
    </section>

<?php
get_footer();

