<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action('shop_wp_footer_parts', 'shop_wp_newsletter', 10);
function shop_wp_newsletter() {
	get_template_part('template-parts/footer/newsletter');
}
add_action('shop_wp_footer_parts', 'shop_wp_open_footer_div', 15);
function shop_wp_open_footer_div(){
	get_template_part('template-parts/footer/open-footer-div');
}
add_action('shop_wp_footer_parts', 'shop_wp_widgets', 20);
function shop_wp_widgets() {
	get_template_part('template-parts/footer/widgets');
}

add_action('shop_wp_footer_parts', 'shop_wp_copyright', 30);
function shop_wp_copyright() {
	get_template_part('template-parts/footer/copyright');
}
add_action('shop_wp_footer_parts', 'shop_wp_close_footer_div', 35);
function shop_wp_close_footer_div(){
	get_template_part('template-parts/footer/close-footer-div');
}