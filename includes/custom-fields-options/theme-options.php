<?php
if ( ! defined( 'ABSPATH') ) {
     exit;
 }
 
use Carbon_Fields\Container;
use Carbon_Fields\Field;

// Add second options page under 'Basic Options'
Container::make( 'theme_options', __( 'Настройки темы' ) )
    ->set_icon( 'dashicons-carrot' )
    ->add_tab( __( ' Шапка' ), array(
	    Field::make( 'select', 'shop_wp_header_logic_logo', 'Будет ли лого?' )
        ->add_options(array(
        	'yes' => 'Будет',
	        'no'  => 'Не будет'
        )),
	    Field::make( 'image', 'shop_wp_header_logo', 'Логотип' )
        ->set_conditional_logic(array(
        	'relation' => 'AND',
	        array(
		        'field'   => 'shop_wp_header_logic_logo',
		        'value'   => 'yes',
		        'compare' => '=',
	        )
        )),
        Field::make( 'text', 'shop_wp_header_name', __( 'Название' ) )
            ->set_width(30)
	        ->set_conditional_logic(array(
		        'relation' => 'AND',
		        array(
			        'field'   => 'shop_wp_header_logic_logo',
			        'value'   => 'no',
			        'compare' => '=',
		        )
	        )),
        Field::make( 'text', 'shop_wp_header_descr', __( 'Описание' ) )
            ->set_width(30)
	        ->set_conditional_logic(array(
		        'relation' => 'AND',
		        array(
			        'field'   => 'shop_wp_header_logic_logo',
			        'value'   => 'no',
			        'compare' => '=',
		        )
	        )),
    ) )
    ->add_tab( __( 'Подвал' ), array(
        Field::make( 'text', 'shop_wp_footer_copy', 'Копирайт' )
            ->set_default_value( 'Shoper is made with love in Warsaw, 2014 © All rights reserved. El Passion')
            ->set_width(50),
        Field::make( 'radio', 'shop_wp_footer_newsletter_show', __( 'Показать блок подписки' ) )
	        ->set_width(50)
	        ->add_options( array(
		         'on' => 'включить',
		         'off'  => 'выключить'
	        ) ),
    )  );


