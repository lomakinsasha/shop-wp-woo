<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

register_nav_menus(array(
	'primary'  => 'Основное',
));

function shop_wp_header_menu() {
	wp_nav_menu( array(
		'theme_location'  => 'primary',
		'menu'            => '',
		'container'       => 'ul',
		'menu_class'      => 'menu justify-content-center d-flex navbar-nav mr-auto',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'items_wrap'      => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => '',
	) );
}

add_filter('widget_nav_menu_args', 'shop_wp_add_classes_widget_footer', 10 );
function shop_wp_add_classes_widget_footer($nav_menu_args) {
	$nav_menu_args['menu_class'] = 'navigation__footer';
	return $nav_menu_args;
}