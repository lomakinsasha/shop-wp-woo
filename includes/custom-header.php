<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

add_action('header_parts', 'shop_wp_nav', 10);
function shop_wp_nav() {
    get_template_part('template-parts/header/nav');
}
add_action('header_parts', 'shop_wp_modal_login', 20);
function shop_wp_modal_login() {
	get_template_part('template-parts/header/modal-login');
}
add_action('header_parts', 'shop_wp_modal_register', 30);
function shop_wp_modal_register() {
	get_template_part('template-parts/header/modal-register');
}