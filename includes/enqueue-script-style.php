<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Enqueue scripts and styles.
 */


add_action( 'wp_enqueue_scripts', 'shop_wp_woo_styles' );
function shop_wp_woo_styles() {
    wp_enqueue_style( 'shop-wp-woo-style', get_stylesheet_uri(), array('shop-wp-bootsrap-style', 'shop-wp-awesome-style', 'shop-wp-fonts', 'shop-wp-woocommerce-style') );
	wp_enqueue_style( 'shop-wp-bootsrap-style', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null, 'all' );
	wp_enqueue_style('shop-wp-awesome-style', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('shop-wp-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,700');


    function shop_wp_style_enqueue_category_template_styles() {
    if ( is_page_template( 'category-template.php' ) ) {
        wp_enqueue_style( 'shop-wp-category-style', get_template_directory_uri() . '/assets/css/category.css' );
    }
	}
	add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_category_template_styles' );

	// Enqueue style to Contacts
	function shop_wp_style_enqueue_contact_template_styles() {
	    if ( is_page_template( 'contacts-template.php' ) ) {
	        wp_enqueue_style( 'shop-wp-contact-style', get_template_directory_uri() . '/assets/css/contact.css' );
	    }
	}
	add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_contact_template_styles' );



	// Enqueue style to About
	function shop_wp_style_enqueue_about_template_styles() {
	   if ( is_page_template( 'about-template.php' ) ) {
	       wp_enqueue_style( 'shop-wp-about-style', get_template_directory_uri() . '/assets/css/about.css' );
	   }
	}
	add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_about_template_styles' );

	// Enqueue style to Category-product


	// Enqueue style to Details || single-post
	function shop_wp_style_enqueue_details_template_styles() {
	    if ( is_page_template( 'details-template.php' ) || is_page_template( 'single-post.php' ) ) {
	        wp_enqueue_style( 'shop-wp-details-style', get_template_directory_uri() . '/assets/css/details.css' );
	    }
	}
	add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_details_template_styles' );

}

add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_category_product_template_styles' );
function shop_wp_style_enqueue_category_product_template_styles() {
	if ( is_archive() ) {
		wp_enqueue_style( 'shop-wp-category-product-style', get_template_directory_uri() . '/assets/css/category-product.css' );
	}
}

// Enqueue style to Cart
function shop_wp_style_enqueue_cart_template_styles() {
	if ( is_cart() || is_checkout() ) {
		wp_enqueue_style( 'shop-wp-cart-style', get_template_directory_uri() . '/assets/css/cart.css' );
	}
}
add_action( 'wp_enqueue_scripts', 'shop_wp_style_enqueue_cart_template_styles' );

add_action( 'wp_enqueue_scripts', 'shop_wp_woo_scripts' );
function shop_wp_woo_scripts() {

    wp_enqueue_script( 'shop-wp-woo-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'shop-wp-woo-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'shop-wp-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true );
    wp_enqueue_script( 'shop-wp-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'shop-wp-slick-js', get_template_directory_uri() . '/assets/slick/slick.min.js', array('jquery'), null, true );
	wp_enqueue_script( 'shop-wp-ajax-search-js', get_template_directory_uri() . '/assets/js/ajax-search.js' , array('jquery'), null, true );
	wp_enqueue_script('ajax-search' , get_template_directory_uri() . '/assets/js/ajax-search.js', array('jquery'), null, true);
	wp_localize_script('ajax-search', 'search_form' , array(
		'url' => admin_url( 'admin-ajax.php' ),
		'nonce' => wp_create_nonce('search-nonce')
	));
	wp_enqueue_script( 'shop-wp-main-js', get_template_directory_uri() . '/assets/js/main.js' , array('jquery'), null, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
