<?php
 if ( ! defined( 'ABSPATH') ) {
     exit;
 }

 add_action( 'widgets_init', 'shop_wp_woo_widgets_init' );
 function shop_wp_woo_widgets_init() {
     register_sidebar( array(
         'name'          => esc_html__( 'Sidebar', 'shop-wp-woo' ),
         'id'            => 'sidebar-1',
         'description'   => esc_html__( 'Add widgets here.', 'shop-wp-woo' ),
         'before_widget' => '<section id="%1$s" class="widget %2$s">',
         'after_widget'  => '</section>',
         'before_title'  => '<h2 class="widget-title">',
         'after_title'   => '</h2>',
     ) );
	 register_sidebar( array(
		 'name'          => 'Подвал левый',
		 'id'            => 'footer-1',
		 'before_widget' => '<section id="%1$s" class="widget navigation__footer %2$s">',
		 'after_widget'  => '</section>',
		 'before_title'  => '<h3 class="title-footer">',
		 'after_title'   => '</h3>',
	 ) );
	 register_sidebar( array(
		 'name'          => 'Подвал цетральный',
		 'id'            => 'footer-2',
		 'before_widget' => '<section id="%1$s" class="widget navigation__footer %2$s">',
		 'after_widget'  => '</section>',
		 'before_title'  => '<h3 class="title-footer">',
		 'after_title'   => '</h3>',
	 ) );
	 register_sidebar( array(
		 'name'          => 'Подвал правый',
		 'id'            => 'footer-3',
 		 'before_widget' => '<section id="%1$s" class="widget navigation__footer %2$s">',
		 'after_widget'  => '</section>',
		 'before_title'  => '<h3 class="title-footer">',
		 'after_title'   => '</h3>',
	 ) );
 }

