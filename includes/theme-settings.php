<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

 if ( ! function_exists( 'shop_wp_woo_setup' ) ) :

     add_action( 'after_setup_theme', 'shop_wp_woo_setup' );

     function shop_wp_woo_setup() {

         // Add default posts and comments RSS feed links to head.
         add_theme_support( 'automatic-feed-links' );
         add_theme_support( 'title-tag' );
         add_theme_support( 'post-thumbnails' );
         add_theme_support( 'html5', array(
             'search-form',
             'comment-form',
             'comment-list',
             'gallery',
             'caption',
         ) );

         // Add theme support for selective refresh for widgets.
         add_theme_support( 'customize-selective-refresh-widgets' );
         add_theme_support( 'custom-logo', array(
             'height'      => 43,
             'width'       => 43,
             'flex-width'  => true,
             'flex-height' => true,
         ) );

         add_theme_support( 'woocommerce' );
//         add_theme_support( 'wc-product-gallery-zoom' );
         add_theme_support( 'wc-product-gallery-lightbox' );
         add_theme_support( 'wc-product-gallery-slider' );
     }
 endif;

 add_action( 'after_setup_theme', 'shop_wp_woo_content_width', 0 );
 function shop_wp_woo_content_width() {
     $GLOBALS['content_width'] = apply_filters( 'shop_wp_woo_content_width', 640 );
 }

