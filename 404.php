<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package shop-wp-woo
 */

get_header();
?>
<div class="container">
    <div class="row">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                <section class="error-404 not-found">
                    <header class="page-header mt-5 mb-5">
                        <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'shop-wp-woo' ); ?></h1>
                    </header><!-- .page-header -->

                    <div class="page-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'shop-wp-woo' ); ?></p>

	                            <?php
	                            get_search_form();

	                            the_widget( 'WP_Widget_Recent_Posts' );
	                            ?>
                            </div>
                            <div class="col-lg-4">
                                <div class="widget widget_categories">
                                    <h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'shop-wp-woo' ); ?></h2>
                                    <ul>
			                            <?php
			                            wp_list_categories( array(
				                            'orderby'    => 'count',
				                            'order'      => 'DESC',
				                            'show_count' => 1,
				                            'title_li'   => '',
				                            'number'     => 10,
			                            ) );
			                            ?>
                                    </ul>
                                </div><!-- .widget -->
                            </div>
                            <div class="col-lg-4">
	                            <?php
	                            /* translators: %1$s: smiley */
	                            $shop_wp_woo_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'shop-wp-woo' ), convert_smilies( ':)' ) ) . '</p>';
	                            the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$shop_wp_woo_archive_content" );

	                            the_widget( 'WP_Widget_Tag_Cloud' );
	                            ?>
                            </div>
                        </div>
                    </div><!-- .page-content -->
                </section><!-- .error-404 -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>
</div>
<?php
get_footer();
