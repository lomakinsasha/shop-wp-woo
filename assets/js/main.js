jQuery(document).ready(function($){
  

  $(".fa-search").click(function(){
    $(".container-x, .input").toggleClass("active");
    $("input[type='text']").focus();
  });
  $('.autoplay').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
  prevArrow: $('.prev-n'),
  nextArrow: $('.next-n'),
});
  $('.shipping-address').hide();

if ($(window).width() > "1000") {
    $(document).on('mouseenter', '.block-cart', function () {
        $('.top-block').css("border", "1px solid #e5e5e5");
        $('.top-block').css("border-bottom", "0");
        $('.bot-block').css("display", "flex");
    });
    $(document).on('mouseleave', '.block-cart', function () {
          $('.bot-block').css("display", "none");
          $('.top-block').css("border", "0");
    });
    $(document).on('mouseleave', '.bot-block', function () {
        $('.top-block').css("border", "1px solid #e5e5e5");
        $('.top-block').css("border-bottom", "0");
        $('.bot-block').css("display", "flex");
    });
}

$('.other-image__galery').mouseover(function() {
  $(this).css("border", "1px solid black");
});

$('.other-image__galery').mouseout(function() {
  $(this).css("border", "1px solid #e5e5e5");
});

$('.other-image__galery').click(function() {
  $(this).css("border", "1px solid pink");
  var images = $(this).attr('src');
  $('.main-image__galery').attr('src', images);
  return false;
});

$('.order-now').click(function(){
  $('.open-icon').css('display', 'flex');
  $('.block-order').css('display', 'none');
  $('.shipping-address').fadeIn(1000);
  return false
});

$('#log-in').click(function(){
    $('.sign').fadeIn(1000);
    return false;
});
$('.close').click(function(){
    $('.sign').fadeOut(1000);
    $('.register-new-user').fadeOut(1000);
    return false;
});
$('#new-user').click(function(){
    $('.sign').fadeOut(1000);
    $('.register-new-user').fadeIn(1000);
    return false;
});
$('#to-reg').click(function () {
    event.preventDefault();
    var id  = $(this).attr('href'),
        top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});
}); 
  