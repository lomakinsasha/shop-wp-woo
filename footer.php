<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shop-wp
 */

?>

<?php
/**
 * @hooked shop_wp_newsletter 10
 * @hooked shop_wp_open_footer_div 15
 * @hooked shop_wp_widgets 20
 * @hooked shop_wp_copyright 30
 * @hooked shop_wp_close_footer_div 35
 */
do_action('shop_wp_footer_parts');
?>

<?php wp_footer(); ?>

</body>
</html>

