<?php

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    load_template( get_template_directory() . '/includes/carbon-fields/vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}

add_action( 'carbon_fields_register_fields', 'shop_wp_register_custom_fields');
function shop_wp_register_custom_fields() {
    require get_template_directory() . '/includes/custom-fields-options/metabox.php';
    require get_template_directory() . '/includes/custom-fields-options/theme-options.php';
}

/**
 * Include start settings
 */
require get_template_directory() . '/includes/theme-settings.php';


/**
 * Include widget areas
 */
require get_template_directory() . '/includes/widget-areas.php';


/**
 * Include scripts and styles
 */
require get_template_directory() . '/includes/enqueue-script-style.php';
/**
*
*/
require get_template_directory() . '/includes/helpers.php';

/**
 * Implement the Custom Header.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Implement the Custom Footer.
 */
require get_template_directory() . '/includes/custom-footer.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Include ajax settings
 */
require get_template_directory() . '/includes/ajax.php';

/**
 * Include register menu
 */
require get_template_directory() . '/includes/navigations.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/includes/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/includes/woocommerce.php';
    require get_template_directory() . '/woocommerce/includes/wc-functions.php';
    require get_template_directory() . '/woocommerce/includes/wc-functions-single.php';
    require get_template_directory() . '/woocommerce/includes/wc-functions-remove.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-cart.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-archive.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-checkout.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-edit-address.php';
}

